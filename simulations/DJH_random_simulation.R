###############################################################################
# This script contains code/functions to run simulations of random guessing
# on the JRD task; e.g., to investigate: 1) the effects of the direction of
# responses, 2) the effects of implementing a threshold/cutoff of angular
# error, 3) the combined influence of 1 and 2.
#
# If you use or re-use this code, then please recognize our work by citing:
#
# Derek J. Huffman and Arne D. Ekstrom (2019) Which way is the bookstore? A 
# closer look at the judgments of relative directions task, Spatial Cognition 
# and Computation, 19:2, 93-129, DOI: 10.1080/13875868.2018.1531869
#
# WRITTEN BY DEREK J HUFFMAN (COPYRIGHT DEREK J HUFFMAN 2019)
###############################################################################

# load the relevant package(s) ------------------------------------------------
library(gtools)  # used for the permutations() function


###############################################################################
# Function for running random simulation of guesses. Allows you to pass in ----
# the number of JRD questions and the relative constraints of the angles of ---
# "responses". Note: the constrainAnglesBw0and360 line deals with the ---------
# possibility of using negative angles, e.g., -90 to 90 for simulated ---------
# responses in the forward-facing 180 (would become 0 to 90 and 270 to 360). --
###############################################################################
rand_resp_sim <- function(num_qs, min_ang, max_ang) {
  # simulation w/ numQs elements constrained guesses b/w minAng and maxAng ----
  angle_arr <- runif(num_qs, min_ang, max_ang)
  # constrain angles b/w 0 and 360 (e.g., -90 becomes 270) and return ---------
  constrain_angles_bw_0_and_360(angle_arr)
}


###############################################################################
# Function to contrain angles to be b/w 0 and 360; i.e., convert negative -----
# angles to be positive angles by adding 360. ---------------------------------
###############################################################################
constrain_angles_bw_0_and_360 <- function(angle_arr) {
  angle_arr[angle_arr < 0] <- 360 + angle_arr[angle_arr < 0]
  angle_arr
}


###############################################################################
# Function to contrain angles to be b/w -180 & 180; i.e., convert angles ------
# greater than 180 to be negative angles by subtracting 360 from them. --------
###############################################################################
constrain_angles_bw_min180_and_180 <- function(angle_arr) {
  angle_arr[angle_arr > 180] <- angle_arr[angle_arr > 180] - 360
  angle_arr
}


###############################################################################
# Function for calculating the error. -----------------------------------------
###############################################################################
calc_error <- function(answers, responses) {
  # calculate absolute error --------------------------------------------------
  error_arr <- abs(answers - responses)
  # update the array to constrain angles to be b/w 0 and 180; the abs() bit ---
  # ensures all pos and return the result -------------------------------------
  abs(constrain_angles_bw_min180_and_180(error_arr))
}


###############################################################################
# Function for implementing a threshold for error; i.e., to exclude "trials" --
# w/ error exceeding a pre-defined threshold. ---------------------------------
###############################################################################
exclude_trials_exceeding_thresh <- function(x, thresh) {
  x[x < thresh]
}


###############################################################################
# Function for calculating the angles between two input stores; convert from --
# radians to degrees with clockwise rotation (for consistency w/ Landmarks). --
###############################################################################
calc_ang_bw_stores <- function(coords1, coords2) {
  ang_rad <- atan2(y = coords1[2] - coords2[2], x = coords1[1] - coords2[1])
  ang_rad * (-180 / pi)
}


###############################################################################
# Function for calculating the answer for a JRD question. Argument rowInds is -
# a row-index array of length 3 (i.e., 3 stores) and coordMat is a matrix and -
# it is expected to have 2 columns and nrows(coordMat) is equal to the number -
# of stores (i.e., each row represents the x,y coords for a store). -----------
###############################################################################
calc_ans <- function(row_inds, coord_mat) {
  # calculate the two angles --------------------------------------------------
  a <- calc_ang_bw_stores(coord_mat[row_inds[1], ], coord_mat[row_inds[2], ])
  b <- calc_ang_bw_stores(coord_mat[row_inds[1], ], coord_mat[row_inds[3], ])
  # calculate the relative angle ----------------------------------------------
  c <- b - a
  # scale to be b/w 0 and 360 -------------------------------------------------
  c <- constrain_angles_bw_0_and_360(c)
  # return the relative angle -------------------------------------------------
  c
}


###############################################################################
# Function to calculate the relative angles (i.e., the answers) for all -------
# permutations of quesions based on argument storeCoords. ---------------------
#
# Note: my solution here was to generate all of the possible permutation ------
# indexes and to then use those indexes in an apply call to loop over all -----
# possible questions, which IMHO is much, much cleaner than using nested for --
# loops (as was done in the Landmarks code). ----------------------------------
#
# Note well: this code depends on the gtools package. -------------------------
#
# RETURN:
# An array with the answers from all possible questions based on all ----------
# permutations of JRD questions from the argument storeCoords. ----------------
###############################################################################
calc_ans_all_qs <- function(store_coords) {
  # gather all of the unique permutations (r = 3: 3 stores per question) ------
  perm_matrix <- permutations(n = nrow(store_coords), r = 3)
  # apply the calcAns function over all of the rows (i.e., permutations) ------
  apply(perm_matrix, MARGIN = 1, FUN = calc_ans, coord_mat = store_coords)
}


###############################################################################
# Function for running a random simulation for a single subject. --------------
###############################################################################
sim_sub <- function(city_angs, num_qs, min_ang, max_ang, summ_stat = mean, 
                    thresh = 180) {
  # simulate responses --------------------------------------------------------
  resps <- rand_resp_sim(num_qs, min_ang = min_ang, max_ang = max_ang)
  # if num_qs differs from the number of possible questions (i.e., the --------
  # length of city_angs), then randomly select num_qs of the answers ----------
  if (num_qs != length(city_angs)) {
    city_angs <- sample(city_angs, size = num_qs)
  }
  # calculate error for simulated resps --------------------------------------
  sim_err <- calc_error(answers = city_angs, responses = resps)
  # implement error threshold (note: nothing happens unless thresh < 180) ----
  sim_err_thresh <- exclude_trials_exceeding_thresh(sim_err, thresh = thresh)
  # calculate the summStat (e.g., median, mean) and return the result --------
  summ_stat(sim_err_thresh)
}


###############################################################################
# Function for running multiple (e.g., "subjects": Ss) random simulations.
#
# ARGUMENTS:
# city_file --> a file that contains the coordinates of each store from
#               a city (expects that each row is a store and the cols
#               are the x,y coordinates). Columns should be delimited by
#               whitespace (i.e., space[s] or tab[s])    
#
# min_ang ----> the minimum angle of response (note: can be negative)
#
# max_ang ----> the maximum angle of response (note: can be negative but 
#               should be > min_ang; e.g., min_ang = -100, max_ang = -25)
#
# n_sub ------> number of subjects to simulate (default: 10000)
#
# num_qs -----> the number of questions to simulate (default: all
#               permutations of possible questions). note: this is likely
#               confusing, but b/c R implements lazy evaluation, it will let
#               you pass in things that are not named until the body of the
#               function; thus, here it will allow length(city_angs_all_qs) 
#               as the default b/c it will not evaulate this until it needs 
#               to;i.e., when numQs is called in the function (in the 
#               replicate call), which is after city_angs_all_qs is defined.
#
# thresh -----> the threshold of error to exclude in the simulations
#               (default: 180 degrees; i.e., include all trials)
#
#
# RETURN: 
# An array that represents the distribution of random guessing performance
# based on the input arguments to the function.
###############################################################################
sim_mult_ss <- function(city_file, min_ang, max_ang, n_sub = 10000, 
                        summ_stat = mean, num_qs = length(city_angs_all_qs),
                        thresh = 180) {
  # load the city coordinates -------------------------------------------------
  city_coords <- as.matrix(read.table(city_file))
  # calculate the angles of the answers ---------------------------------------
  city_angs_all_qs <- calc_ans_all_qs(city_coords)
  # run the sim_sub function n_sub times --------------------------------------
  replicate(n = n_sub, 
            sim_sub(city_angs = city_angs_all_qs, num_qs = num_qs, 
                    min_ang = min_ang, max_ang = max_ang, 
                    summ_stat = summ_stat, thresh = thresh))
}

