This repository contains code used in Huffman and Ekstrom (2019, Spatial Cognition & Computation). Specifically, it contains code (written in R) for running both the within subject and group-level permutation analyses described in the paper (i.e., Sections 2.1.6 and 5.1.2, respectively), as well as code for running the simulations (i.e., Section 4).

These files can be found in the following folders:

permutations
simulations

To run the simulations, please see the "parent" file DJH_run_simulations_and_plot_fig7.R Please read the top of this script for more information. Below I provide a brief overview of using the simulation functions.

Note, the main function for the simulations is located in the "child" script DJH_random_simulations.R Specifically, to run this function, you can call the sim_mult_ss function as shown in DJH_run_simulations_and_plot_fig7.R For example, to simulate random guesses in front of the participant, you could do the following:

sim_min90_to_90 <- sim_mult_ss(city_file = cf, min_ang = -90, max_ang = 90)

This function is set to include a default maximum absolute angular error threshold of 180 degrees (i.e., no eliminated trials). To simulate the effect of a maximum angular error (e.g., 90 degrees) with responses drawn from a uniform distribution:

sim_unif_thresh90 <- sim_mult_ss(city_file = cf, min_ang = 0, max_ang = 360, thresh = 90)

Likewise, to investigate the combined effects of restricted responses (e.g., between -90 and 90 degrees) and a maximum angular error cutoff (e.g., 90 degrees):

sim_min90_to_90_thresh90 <- sim_mult_ss(city_file = cf, min_ang = -90, max_ang = 90, thresh = 90)

If you have any questions or comments about this code or our paper, then please email Derek J. Huffman at djhuffman AT ucdavis DOT edu (please replace AT and DOT with appropriate symbols).

If you use or re-use this code, then please recognize our work by citing:

Derek J. Huffman and Arne D. Ekstrom (2019) Which way is the bookstore? A closer look at the judgments of relative directions task, Spatial Cognition and Computation, 19:2, 93-129, DOI: 10.1080/13875868.2018.1531869

ALL CODE WRITTEN BY DEREK J HUFFMAN (COPYRIGHT DEREK J HUFFMAN 2019)